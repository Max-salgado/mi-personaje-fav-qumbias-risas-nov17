<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mi personaje favorito de ‘Qumbias y Risas’ de Edwin Sierra</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/main.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/estilos-min.css?v4">

	<link rel="icon" type="image/png" href="https://radionuevaq.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
</head>
<body>

	<?php include ('includes/superior.php'); ?>
	



	<section id="contenedor-principal">

		<article class="contenedor-interno container-fluid">
			

			<header class="cp-superior row"> 

				<div class="logo-q col-md-1 col-sm-1">
					<a href="https://radionuevaq.pe/" target="_blank"><img src="assets/img/logo-nuevaq.png" alt="Logo Nueva Q" class="img-responsive center-block"></a>
				</div>

				<div class="logo-campania col-md-5 col-sm-11">
					<a href="http://concursos.crp.pe/nuevaq/campanias/minisites/mi-personaje-fav-qumbias-risas-nov17/"><img src="assets/img/logo-campania.png" alt="Logo de la campaña" class="img-responsive center-block"></a>
				</div>




				<div class="enunciado-principal col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-1">
					<h1 class="titulo">
						<img src="assets/img/titulo-campania.png" alt="Título campaña" class="img-responsive center-block">
					</h1>
				
					<p class="parrafo parrafo1">Vota por tu <span> personaje favorito</span> </p>
				
				</div>



				<div class="premios-principal col-md-2 col-sm-2">
					<img src="assets/img/premios.png" alt="Premios" class="img-responsive center-block">					
				</div>


			</header> <!-- fin cp-superior -->



			<div class="cp-inferior col-md-10 col-md-offset-1">
				
				<a href="https://radionuevaq.pe/concursos/vota-por-tu-personaje-favorito-de-edwin-sierra-en-qumbias-y-risas-y-gana-premios-15708"><p class="texto-enunciado-bajada reseteo">Conoce a los ganadores aquí</p></a>
			</div> <!-- fin cp-inferior -->
		
		</article>
	</section> <!-- fin contenedor-principal -->





	<?php include ('includes/pie.php'); ?>
	<?php include ('includes/contenedor-video.php'); ?>
	<?php include ('includes/tyc.php'); ?>
    

	<footer id="pie">
		<div class="contenedor-interno"></div>
	</footer> <!-- pie -->
	
<!-- Begin comScore DAx STANDARD-->
<script type="text/javascript">
  // <![CDATA[
  function udm_(a){var b="comScore=",c=document,d=c.cookie,e="",f="indexOf",g="substring",h="length",i=2048,j,k="&ns_",l="&",m,n,o,p,q=window,r=q.encodeURIComponent||escape;if(d[f](b)+1)for(o=0,n=d.split(";"),p=n[h];o<p;o++)m=n[o][f](b),m+1&&(e=l+unescape(n[o][g](m+b[h])));a+=k+"_t="+ +(new Date)+k+"c="+(c.characterSet||c.defaultCharset||"")+"&c8="+r(c.title)+e+"&c7="+r(c.URL)+"&c9="+r(c.referrer),a[h]>i&&a[f](l)>0&&(j=a[g](0,i-8).lastIndexOf(l),a=(a[g](0,j)+k+"cut="+r(a[g](j+1)))[g](0,i)),c.images?(m=new Image,q.ns_p||(ns_p=m),m.src=a):c.write("<","p","><",'img src="',a,'" height="1" width="1" alt="*"',"><","/p",">")}
  udm_('http'+(document.location.href.charAt(4)=='s'?'s://sb':'://b')+'.scorecardresearch.com/p?c1=2&c2=6906600&ns_site=nuevaq-radio&name=concursos.minisites.personaje-favorito-qumbias-risas-nov17.ganadores');
  // ]]>
</script>
<noscript>
  <p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6906600&amp;ns_site=nuevaq-radio&amp;name=concursos.minisites.personaje-favorito-qumbias-risas-nov17.ganadores" height="1" width="1" alt="*"></p>
</noscript>
<!-- End comScore DAx STANDARD -->
<script language="JavaScript1.3" src="http://b.scorecardresearch.com/c2/6906600/ct.js"></script>
<script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39226895-1', 'auto');
  ga('send', 'pageview');
</script> 


</body>
</html>



