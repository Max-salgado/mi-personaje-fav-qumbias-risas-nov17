<?php
require_once("includes/conn.php");
require_once("includes/tools.php");
require_once("includes/emblue.php");
require_once("includes/encode.php");
session_start();


$tool = new Tools();

$action = $_POST['action'];
$campania = 366;
$fecreg = date("Y-m-d");
$horreg = date("H:i:s");
$fecha = date("Y-m-d H:i:s");


switch ($action) {

    case "registro":
        //Variables
        $nombres = $_POST['nombres'];  
        $telefono = $_POST['telefono'];       
        $dni = $_POST['dni'];        
        $email = $_POST['email'];
        $departamento = $_POST['departamento'];
        $id_categoria = $_POST['id_categoria'];
        $id_nominado = $_POST['id_nominado'];
        $genero = $_POST['genero'];
        $dia = $_POST['dia'];
        $mes = $_POST['mes'];
        $anio = $_POST['anio'];
        $fechayhora = mktime(0, 0, 0, $mes, $dia, $anio);
        $fechayhora = date('Y-m-d h:i:s', $fechayhora);

        if(empty($_POST['informacion']))
        {
            $boletin = 0;
        }
        else
        {
            $boletin = 1;
        }

        //Validar Campos
        if(!empty($nombres) && !empty($telefono) && !empty($dni))
        {
            if(!empty($email) && !empty($dni))
            {
                $id_inscrito = $tool->valInsrito($dni, $email, $campania);

                if ($id_inscrito == 0) 
                {
                    $inscrito['nombre'] = mysql_real_escape_string(utf8_decode($nombres));
                    $inscrito['dni'] = mysql_real_escape_string($dni);
                    $inscrito['email'] = mysql_real_escape_string($email);
                    $inscrito['telefono'] = mysql_real_escape_string($telefono);
                    $inscrito['fecha_registro'] = $fecreg;
                    $inscrito['hora_registro'] = $horreg;
                    $inscrito['fecha_nacimiento'] = $fechayhora;
                    $inscrito['campania_id'] = mysql_real_escape_string($campania);
                    $inscrito['acepto_boletin'] = mysql_real_escape_string($boletin);
                    $inscrito['acepto_tyc'] = 1;
                    $inscrito['estado'] = 1;
                    $id_inscrito = $tool->insert("inscritos", $inscrito);

                    if($boletin == 1)
                    {
                       // Emblue
                        $emblue = new Emblue();
                        $user_emblue = $emblue->addContact($id_campania, $nombres, $email, $apellidos, $telefono, $dni, $genero, $fechayhora);

                        $datos_emblue = array(
                            'id_inscrito' => $id_inscrito,
                            'id_campania' => $id_campania,
                            'id_emblue' => $user_emblue->EmailId,
                            'estado' => $user_emblue->Description,
                            'respuesta' => $user_emblue->ContactoEstadoId
                        );
                        $tool->insert("inscritos_detalle", $datos_emblue); 
                    }
                    

                    // Mail de Agradecimiento
                    //$mail = $emblue->sendMail($user_emblue->EmailId, $nombres);

                    // Guardar votos
                    if(!empty($_POST['respuesta'])) {
                        foreach($_POST['respuesta'] as $respuesta) {
                            $tool->registrarVoto($id_inscrito, $campania, $respuesta, 1, $fecreg, $horreg);
                        }
                    }
                    header("Location: gracias.php");


                    
                }
                else
                {                   
                    header("Location: index.php?msg=limite");  
                }
            }
        }
        else
        {
           header("Location: index.php");
        }
    break;

    default:
        header("Location: index.php");
    break;
}
?>
