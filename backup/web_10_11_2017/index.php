<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/main.js?v8"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/estilos-min.css?v2">


</head>
<body>
	<!-- <div class="parche"></div> -->
	<?php include ('includes/superior.php'); ?>
	



	<section id="contenedor-principal">

		<article class="contenedor-interno container-fluid">
			

			<header class="cp-superior row"> 

				<div class="logo-q col-md-1 col-sm-1">
					<a href="https://radionuevaq.pe/" target="_blank"><img src="assets/img/logo-nuevaq.png" alt="Logo Nueva Q" class="img-responsive center-block"></a>
				</div>

				<div class="logo-campania col-md-5 col-sm-11">
					<a href="http://concursos.crp.pe/nuevaq/campanias/minisites/mi-personaje-fav-qumbias-risas-nov17/"><img src="assets/img/logo-campania.png" alt="Logo de la campaña" class="img-responsive center-block"></a>
				</div>




				<div class="enunciado-principal col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-1">
					<h1 class="titulo">
						<img src="assets/img/titulo-campania.png" alt="Título campaña" class="img-responsive center-block">
					</h1>
				
					<p class="parrafo parrafo1">Vota por tu <span> personaje favorito</span> de <span>Edwin Sierra</span></p>
					<p class="parrafo parrafo2">Puedes elegir a más de uno</p>
				
				</div>



				<div class="premios-principal col-md-2 col-sm-2">
					<img src="assets/img/premios.png" alt="Premios" class="img-responsive center-block">					
				</div>


			</header> <!-- fin cp-superior -->



			<div class="cp-inferior col-md-10 col-md-offset-1">
				
				<form>

					<div class="margen-inferior">

						<div class="contenedor-personaje">
							
							<div class="imagen-personaje" data-target="#mostrar-video1" data-toggle="modal">
								<img src="assets/img/personajes/1.png" alt="Cantinflas" class="img-responsive center-block">
							</div>

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>

						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video2" data-toggle="modal">
								<img src="assets/img/personajes/2.png" alt="Choledo" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>


						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video3" data-toggle="modal">
								<img src="assets/img/personajes/3.png" alt="El Chino" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>


						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video4" data-toggle="modal">
								<img src="assets/img/personajes/4.png" alt="El Patrón" class="img-responsive center-block">
								
								

							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>


						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video5" data-toggle="modal">
								<img src="assets/img/personajes/5.png" alt="La Fuana" class="img-responsive center-block">
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>
					</div>

					<!-- /**/ -->
					
					<div>
						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video6" data-toggle="modal">
								<img src="assets/img/personajes/6.png" alt="Lalito" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>




						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video7" data-toggle="modal">
								<img src="assets/img/personajes/7.png" alt="La tía Peta" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>




						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video8" data-toggle="modal">
								<img src="assets/img/personajes/8.png" alt="La Fuana" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>



						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video9" data-toggle="modal">
								<img src="assets/img/personajes/9.png" alt="La Fuana" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>





						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video10" data-toggle="modal">
								<img src="assets/img/personajes/10.png" alt="La Fuana" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="radio" name="respuesta" class="seleccionados" value="1">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>
					</div>

				</form>

			</div> <!-- fin cp-inferior -->
			
			<div class="contenedor-legal">
				<a href="#"  data-target="#TermsCond" data-toggle="modal">Ver términos y condiciones</a><p> | SORTEO 29 DE NOVIEMBRE</p>
			</div>
			
		</article>
	</section> <!-- fin contenedor-principal -->




	<div id="formulario">
		<div class="contenedor-interno">
			<div class="contenedor-formulario row">

				<form class="col-md-10 col-md-offset-1">

					<div class="caja-form col-md-4 col-sm-6">
						<label>Nombres:</label>
						<input class="data-usuario" placeholder="Coloca tus nombres" type="text" >
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<label>Apellidos:</label>
						<input class="data-usuario" placeholder="Coloca tus apelldios" type="text" >
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<label>Dni:</label>
						<input class="data-usuario" placeholder="Coloca tu Dni" type="number">
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<label>E-mail:</label>
						<input class="data-usuario" placeholder="Coloca tu E-mail" type="email">
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<label>Teléfono:</label>
						<input class="data-usuario" placeholder="Coloca tu teléfono" type="tel">
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<button class="btn-data-usuario">
							<p>
								Enviar
							</p>
						</button>
					</div>


					<div class="col-sm-12 aceptar-campos">
			        	<div class="checkbox">
			                  <input type="checkbox" value="1" name="informacion" id="informacion">
			                  <label for="informacion">Deseo recibir información de Radio Planeta y sus anunciantes.</label>

			              </div>
			              <div class="checkbox">
			                  <input type="checkbox" value="1" name="titular" id="titular">
			                  <label for="titular">Acepto los <a href="#" id="termiCond" data-toggle="modal" data-target="#TermsCond">Términos y Condiciones</a><span id="error_tyc"></span></label>
			              </div>
			        </div>

				</form>

			</div> <!-- fin contenedor-formulario -->
		</div>

	</div> <!-- fin formulario -->

	<?php include ('includes/pie.php'); ?>
	<?php include ('includes/contenedor-video.php'); ?>
	<?php include ('includes/tyc.php'); ?>

	<?php include ('includes/limite.php'); ?>
    

	<footer id="pie">
		<div class="contenedor-interno"></div>
	</footer> <!-- pie -->
	


	
</body>
</html>



