<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/main.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/estilos-min.css?v2">


</head>
<body>
	<?php include ('includes/superior.php'); ?>
	



	<section id="contenedor-principal">

		<article class="contenedor-interno container-fluid">
			

			<header class="cp-superior row"> 

				<div class="logo-q col-md-1 col-sm-1">
					<a href="https://radionuevaq.pe/" target="_blank"><img src="assets/img/logo-nuevaq.png" alt="Logo Nueva Q" class="img-responsive center-block"></a>
				</div>

				<div class="logo-campania col-md-5 col-sm-11">
					<a href="http://concursos.crp.pe/nuevaq/campanias/minisites/mi-personaje-fav-qumbias-risas-nov17/"><img src="assets/img/logo-campania.png" alt="Logo de la campaña" class="img-responsive center-block"></a>
				</div>



				<div class="enunciado-principal col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-1">
					<h1 class="titulo">
						<img src="assets/img/titulo-campania.png" alt="Título campaña" class="img-responsive center-block">
					</h1>
				
					<p class="parrafo parrafo1">Vota por tu <span> personaje favorito</span> de <span>Edwin Sierra</span></p>
					<p class="parrafo parrafo2">Puedes elegir a más de uno</p>
				
				</div>



				<div class="premios-principal col-md-2 col-sm-2">
					<img src="assets/img/premios.png" alt="Premios" class="img-responsive center-block">					
				</div>


			</header> <!-- fin cp-superior -->



			<div class="cp-inferior col-md-10 col-md-offset-1">
				<p class="texto-enunciado">¡Gracias por participar!</p>

			</div> <!-- fin cp-inferior -->
			
			<div class="contenedor-legal">
				<a href="#"  data-target="#TermsCond" data-toggle="modal">Ver términos y condiciones</a>
			</div>
			
		</article>
	</section> <!-- fin contenedor-principal -->





	<?php include ('includes/pie.php'); ?>
	<?php include ('includes/contenedor-video.php'); ?>
	<?php include ('includes/tyc.php'); ?>
    

	<footer id="pie">
		<div class="contenedor-interno"></div>
	</footer> <!-- pie -->
	


	
</body>
</html>



