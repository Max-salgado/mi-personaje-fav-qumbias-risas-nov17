/* useful functions */

function trim(str)
{
    while (str.charAt(0) == ' ')
        str = str.substring(1);
    while (str.charAt(str.length - 1) == ' ')
        str = str.substring(0, str.length - 1);
    return str;
}

function isEmpty(pString) {

    if (trim(pString) == "") {
        return (true);
    }
    else {
        return (false);
    }
}

function isValidNumber(e) {
    ok = "1234567890";
    for (i = 0; i < e.length; i++) {
        if (ok.indexOf(e.charAt(i)) < 0) {
            return (false);
        }
        else
            return (true);
    }
}


function isValidName(e) {
    ok = /^[á-úa-zA-ZÑ\s.\-]+$/;
    if (!e.match(ok))
        return (false);
    else
        return (true);
}

function isEmail(e) {
    ok = "1234567890qwertyuiop[]asdfghjklzxcvbnm.@-_QWERTYUIOPASDFGHJKLZXCVBNM";
    for (i = 0; i < e.length; i++) {
        if (ok.indexOf(e.charAt(i)) < 0) {
            return (false);
        }
    }
    re = /(@.*@)|(\.\.)|(^\.)|(^@)|(@$)|(\.$)|(@\.)/;
    re_two = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!e.match(re) && e.match(re_two)) {
        return (-1);
    }
}



function caracterSpecial(obj) {

    if (obj.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/))
        return (true);

    else
        return (false);

}

function onlyLetter(obj) {
    if (obj.match(/([a-zA-Z])/))
        return (true);
    else
        return (false);

}

function isValue(obj) {
    if (!obj.match(/([1-9])/))
        return (true);
    else
        return (false);

}

function valFecNac(){
    var anio = $("#anio");
    $(".error").remove();

    if (anio.val() == 0) {
        anio.focus().after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        return false;
    }
}

function valRegistro() {

    var nombre = $("#nombres");
    var apellidos = $("#apellidos");
    var dni = $("#dni");
    var email = $("#email");
    var telefono = $("#telefono");    
    var tyc = $("#titular");
   

    var error_mensaje = $("#error_mensaje");
    var error_nombres = $("#error_nombres");
    var error_apellidos = $("#error_apellidos");
    var error_dni = $("#error_dni");
    var error_email = $("#error_email");
    var error_telefono = $("#error_telefono");
    var error_tyc = $("#error_tyc");
    $(".error").remove();

    valPersonaje();

    if (isEmpty(nombre.val())) {
        error_nombres.after("<span class='error'>Es un campo obligatorio</span>");
        nombre.focus();
        return false;
    } else if (caracterSpecial(nombre.val())) {
        error_nombres.after("<span class='error'>No usar caracteres especiales</span>");
        nombre.focus();
        return false;
    } else if (!onlyLetter(nombre.val())) {
        error_nombres.after("<span class='error'>S&oacute;lo pueden ser letras</span>");
        nombre.focus();
        return false;
    }


    if (isEmpty(apellidos.val())) {
        error_apellidos.after("<span class='error'>Es un campo obligatorio</span>");
        apellidos.focus();
        return false;
    } else if (caracterSpecial(apellidos.val())) {
        error_apellidos.after("<span class='error'>No usar caracteres especiales</span>");
        apellidos.focus();
        return false;
    } else if (!onlyLetter(apellidos.val())) {
        error_apellidos.after("<span class='error'>S&oacute;lo pueden ser letras</span>");
        apellidos.focus();
        return false;
    }

    if (isEmpty(email.val())) {
        error_email.after("<span class='error'>Es un campo obligatorio</span>");
        email.focus();
        return false;
    } else if (!isEmail(email.val())) {
        error_email.after("<span class='error'>El email no es correcto</span>");
        email.focus();
        return false;
    }

    if (isEmpty(dni.val())) {
        error_dni.after("<span class='error'>Es un campo obligatorio</span>");
        dni.focus();
        return false;
    } else if (!isValidNumber(dni.val())) {
        error_dni.after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        dni.focus();
        return false;
    }   

    if (isEmpty(telefono.val())) {
        error_telefono.after("<span class='error'>Es un campo obligatorio</span>");
        telefono.focus();
        return false;
    } else if (!isValidNumber(telefono.val())) {
        error_telefono.after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        telefono.focus();
        return false;
    }    

    if(!tyc.is(':checked')){
       error_tyc.after("<span class='error'>Debe aceptar los t&eacute;rminos y condiciones</span>");
       return false;
     }
 


}

function valPersonaje(){
    var personaje = document.getElementsByName("respuesta");
    var cantidad = 0;
   // console.log(personaje.length);

    for (var i = 0; i < personaje.length; i++) {
        var pnj = personaje[i];

        if (pnj.checked) 
        {
          cantidad++;
          if(cantidad > 5)
          {
            pnj.checked = false;
            $(pnj).removeClass( "seleccionado-accion" );
            alert("solo puedes seleccionar 5 personajes");
          }
        }
        
    }
    return cantidad;
}
