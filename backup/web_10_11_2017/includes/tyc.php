<!-- Términos y condiciones MODAL-->
    <div class="modal fade" id="TermsCond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
            <h4 class="modal-title" id="myModalLabel">Términos y Condiciones</h4>
          </div>





          <div class="modal-body">
            <p align="center"><!-- <strong>Justin</strong> -->
     
      
              <span>Planeta, tu música en inglés, te invita al concierto de Justin Bieber de su gira "Purpose Tour" 2017 el 05 de Abril en el Estadio Nacional. Participa, que tú puedes ser unos de los 15 ganadores para asistir a este increíble concierto del artista ícono del pop en inglés del momento.</span>
              <br>
              <br>
            </p>


            <strong><u>Puedes participar de dos formas:</u></strong>
            
            <ol style="margin:4px 0 20px 13px;">

              <li>Ingresa a la web de Radio Planeta <a href="http://planeta.pe/" target="_blank">(planeta.pe)</a>, busca la imagen del Concurso, ingresa al mini sitio y regístrate completando un formulario con tus datos personales (nombres, apellidos, DNI, email, departamento y teléfono). Este registro equivale a un ticket en el ánfora virtual para el sorteo.</li>
              <br>
              <li>Si deseas incrementar tus posibilidades de ganar (multiplicando tu voto) deberás hacer lo siguiente:<br> Estar atento a la programación de radio Planeta, ya que de lunes a domingo nuestros locutores dirán cinco veces al día "la palabra del día". Luego, con "la palabra del día" deberás ingresar a la web de Radio Planeta <a href="http://planeta.pe/" target="_blank">(planeta.pe)</a>, buscar la imagen del Concurso, ingresar al mini sitio, elegir "la palabra del día" entre las opciones que aparecerán y  registrarte completando un formulario con tus datos personales (nombres, apellidos, DNI, email, departamento y teléfono). Cada “palabra del día” ingresada correctamente equivale a cinco (5) “tickets en el ánfora virtual” para el sorteo, por lo que a más “palabras del día” ingresadas correctamente durante la vigencia de la promoción, más oportunidades de ganar. Solo tendrás una oportunidad al día para ingresar la “palabra del día”.
          </li>

            </ol>

            <span>Nota:</span>

            <ul style="margin:4px 0 20px 17px;" type="disc">
              <li>
                Podrás participar hasta las 12:00 pm del día 31 de marzo del 2017.
              </li>

              <li>
                El participante que no proporcione todos los datos personales solicitados y/o éstos no sean actuales, reales, correctos y verdaderos, podrá ser descalificado en cualquier momento de la Promoción, no pudiendo ser en consecuencia acreedor de ningún premio.
              </li>

              <li>
                En caso de detectarse actividad anormal relacionada con el uso del formulario de participación se penalizará a dicho participante cancelando su registro, eliminándolo del concurso sin previo aviso, y bloqueando su participación en el resto de la campaña, no pudiendo ser acreedor de ningún premio. 
              </li>

              <li>
                La relación de los 15 ganadores será publicada el viernes 31 de marzo del 2017 a partir de las 5:00 p.m. en la sección de concursos de la web de Radio Planeta (planeta.pe). No se llamará telefónicamente a ningún ganador.
              </li>
            </ul>


            <p><strong><u>Ámbito de la promoción</u>: </strong>Esta promoción es válida a nivel nacional.</p>
            <br>

            <p><strong><u>Vigencia</u>: </strong>Del miércoles 01 de marzo del 2017 hasta las 12:00pm del día 31 de Marzo del 2017.</p>
            <br>

            <p><strong><u>Fecha del sorteo</u>: </strong>viernes 31 de Marzo del 2017 a la 03:00 pm.</p>
            <br>
            
            <p><strong><u>Premios (Stock)</u>: </strong>15 entradas dobles para el concierto de Justin Bieber de su gira "Purpose Tour", el miércoles 05 de abril del 2017 en el Estadio Nacional. Será una (1) entrada doble por ganador de acuerdo al siguiente detalle:</p>
            <ul style="margin:4px 0 20px 17px;" type="disc">
              <li>4 entradas dobles para la zona Occidente 1</li>
              <li>6 entradas dobles para la zona Campo B</li>
              <li>5 entradas dobles para la zona Campo C</li>
            </ul>
            

             <p><strong><u>Entrega de premios</u>: </strong></p>
             <div style="margin:4px 0 20px 0px;">
               <p>El recojo de los premios se realizará el lunes 03 de Abril del 2017 en calle Juan Nepomuceno Vargas 147, Chorrillos, Lima, <strong style="font-family: GothamBold;">de 2:00pm a 5:00pm. Único día y única hora de entrega</strong>. El ganador deberá acercarse con su DNI original y dos copias.</p>
               <br>
               <p>En caso el ganador no pueda recoger personalmente su premio, puede enviar a un representante con 2 copias del DNI del ganador más el DNI original del representante y una copia del mismo. </p>
               <br>
               <p>La radio no asumirá ningún gasto (sea por transporte, alojamiento, etc.) para el recojo del premio ni para asistir al concierto.</p>
             </div>


             
             <p><strong><u>Entrega de premios</u>: </strong></p>
             <p>La organización y realización del concierto de Justin Bieber en su gira Purpose del 2017 el miércoles 05 de abril del 2017 en el Estadio Nacional, depende de la disponibilidad del artista, no siendo CRP Medios y Entretenimiento SAC responsable de hechos atribuible a éste, que pudieran determinar su imposibilidad de cumplir con dichos compromisos.</p>

             <br>

             <p>La producción, organización y realización del concierto de Justin Bieber en su gira Purpose del 2017 el miércoles 05 de abril del 2017 en el Estadio Nacional, se encuentra a cargo en forma exclusiva y excluyente de la empresa E2 LIVE S.A.C. (RUC N° 20544553055).</p>

          </div>
          <div class="modal-footer"></div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->