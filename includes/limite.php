<!-- Términos y condiciones MODAL-->
    <div class="modal fade" id="limite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
            <!-- <h4 class="modal-title" id="myModalLabel">Términos y Condiciones</h4> -->
          </div>

          <div class="modal-body">
          	<p>Tu voto ya fue registrado, solo puedes votar una vez. ¡Muchas gracias por participar!</p>
          </div>

			<div class="modal-footer"></div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->