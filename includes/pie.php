<div id="pie">
	<footer>
	    <div class="">
	      <div id="footer-wrapper" class="container-fluid">
	          <ul class="logoscrp">
	            <li class="logoradios">
	              <a href="http://ritmoromantica.com/" target="_blank">
	                <img src="assets/img/footer/logo-ritmo-footer.png" alt="ritmo romantica">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.radiolainolvidable.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-lainolvidable-footer.png" alt="la inolvidable">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.moda.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-moda-footer.png" alt="moda">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.radiooasis.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-oasis-footer.png" alt="oasis">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.radiomar.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-radiomar-footer.png" alt="radiomar">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.radionuevaq.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-nuevaq-footer.png" alt="nueva q">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://planeta.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-planeta-footer.png" alt="planeta">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.radioinca.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-inca-footer.png" alt="inca">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.radiomagica.com.pe/" target="_blank">
	                <img src="assets/img/footer/logo-magica-footer.png" alt="magica">
	              </a>
	            </li>
	            <li class="logoradios">
	              <a href="http://www.sonorama.pe/" target="_blank">
	                <img src="assets/img/footer/logo-sonorama-footer.png" alt="sonorama">
	              </a>
	            </li>
	          </ul>

	          <div class="col-sm-2 col-xs-6">
	            <a href="http://www.crpweb.pe/" class="logo-crp" target="_blank">
	              <img src="https://radionuevaq.pe/assets/images/crp-logo.jpg" alt="CRP medios y entretenimiento">
	            </a>
	          </div>
	          <div class="col-sm-10 col-xs-12 TYC">
	            Licenciado por APDAYC  |  Portal auditado por Comscore  |  Portal miembro de IAB  | <a href="http://www.crp.pe/dejanos-musica/index.php?r=crp-5" target="_blank">Déjanos tu música</a> <br>
	            <a href="http://crp.pe/codigoetica.php" target="_blank">Código de Ética</a>  |  <a href="http://crp.pe/terminosycondiciones.php" target="_blank">Términos y Condiciones</a>  |  <a href="http://www.crp.pe/quejasycomunicaciones.php" target="_blank">Reglamento de Soluciones de Quejas</a>  |  <a href="http://www.crp.pe/libro-reclamaciones/" target="_blank">Libro de Reclamaciones</a>
	          </div>
	          <div class="clearfix"></div>
	        
	          <div class="col-xs-12 copyright">
	  		    © Radio Nueva Q - Q Q Qumbia | Radios de Lima | Radios de Perú<br>
	  		    Copyright © CRP Medios y Entretenimiento S.A.C. Todos los derechos reservados
	  		</div>

		  </div> <!--fin static -->

 	</footer>


</div>