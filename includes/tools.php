<?php
require_once("conn.php");

class Tools{

    public function insert($tabla, $campos)    {

        $key = "";
         $value = "";
        foreach ($campos as $k => $v) {
            $key .= $k . ',';
            $value .= ' "' . $v . '",';
        }
        $key = substr($key, 0, strlen($key) - 1);
        $value = substr($value, 0, strlen($value) - 1);
        $query = "INSERT INTO $tabla ($key) VALUES($value)";
        $result = mysql_query($query) or die(mysql_error());

        if ($result) {
            $id =  mysql_insert_id();
            return $id;

        }
    }

    public function update($tabla,$campos,$filter){
        $cad = "";
        foreach($campos as $k=>$v){
            $cad.= $k.'=\''.$v.'\',';
        }
        $cad = substr($cad, 0,strlen($cad)-1);
        $query = "UPDATE $tabla SET $cad WHERE $filter";  
        //die($query);   
        $result = mysql_query($query);
        if($result){
            return true;
        }  
 
    }
  
  function valInsrito($dni, $email, $id_campania){
       
     try
         {
            
      $query = "select id_inscrito from inscritos where  (email ='".$email."' or dni ='".$dni."') and campania_id=".$id_campania;     
      $result = mysql_query($query);
      $existe = 0;
      if (mysql_num_rows($result) > 0)
        $existe = 1;
  
      return $existe;
    
     }catch(Exception $e){ return 0;} 
  }

    function getDepartamento()
    {
        $query = "select id_ubigeo, nombre FROM ubigeos where id_provincia = '0' and id_distrito = '0'";
        $result = mysql_query($query);
        $distrito = "<option value='0'>Departamento</option>";

        while ($row = mysql_fetch_array($result)) {
            $idDistrito = $row['id_ubigeo'];
            $nombreDistrito = utf8_encode($row['nombre']);
            $distrito .= "<option value='" . $idDistrito . "'>" . $nombreDistrito . "</option>";
        }

        return $distrito;
    }


    function valEmail($email, $id_campania){
       
         try
         {
            
            $query = "select id_inscrito from inscritos where email ='".$email."' and campania_id=".$id_campania;         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
    
            if (mysql_num_rows($result) > 0) 
            {
                return $row['id_inscrito'];
            } 
            else
            {
               
              return 0;              
                        
            }
        
       }catch(Exception $e){ return 0;} 
    }

    function getNombres($id)
    {
        
        if(!empty($id) && $id > 0)  
        {
            $query = "select nombre, ap_paterno from inscritos where  id_inscrito = ".$id;         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
            return $row['nombre']. " ". $row['ap_paterno'];
        }
        else
        {
            return "";
        }
        
        
       
    }


    function getCanciones($id_categoria)
    {
        $file = json_decode(file_get_contents('json/canciones.json'));

        if(empty($file))
        {
            $file = json_decode(file_get_contents('json/canciones_backup.json'));
        }

        $lista = array();                
        $button = '';
        $dir = 'medium';        
        
        if(is_array($file))
        {
            shuffle($file);             
            $x = 1;

            foreach ($file as $key => $value)
            {      
              
                
                if($value->id_categoria == $id_categoria)
                {
                                      
                    if(!empty($_SESSION['idins_premios']))
                    {
                        $button = '<form action="action.php" method="post" role="form" id="form">
                        <input id="action" name="action" type="hidden" value="votar"/>
                        <input id="id_nominado" name="id_nominado" type="hidden" value="'.$value->id.'"/>
                        <input id="id_categoria" name="id_categoria" type="hidden" value="'.$id_categoria.'"/>
                        <input id="url" name="url" type="hidden" value="interna"/>

                        <button class="btn btn-vote" data-text="VOTAR AHORA" onclick="javascript: ga(\'send\', \'event\', \'Premios Moda 2017\', \'Votar\', \''.$id_categoria.'\', \''.$value->id.'\');">VOTA AHORA</button>

                        </form>';
                    }
                    else
                    {
                        $button = '<button class="btn btn-vote" data-text="VOTAR AHORA" data-toggle="modal" data-target="#login" onclick="javascript: addNominado('.$value->id.', '.$id_categoria.'); ga(\'send\', \'event\', \'Premios Moda 2017\', \'Votar\', \''.$id_categoria.'\', \''.$value->id.'\');">VOTA AHORA</button>';
                    }

                    if($value->cantidad > 90)
                    {
                        $imagen = $value->imagen_large;
                    }
                    else
                    {
                        $imagen = $value->imagen_medium;
                    }

                   
                    $lista['html'] .= '<li>
                              <!--Item Large-->
                              <figure class="effect-sadie">
                                <img src="artistas/'.$imagen.'?v=1" alt="'.$value->cancion.'">
                                <figcaption>
                                  <h2>
                                    '.$button.'
                                  </h2>
                                    <div class="counter visible-xs">
                                           <span>'.$value->cantidad.'</span>
                                    </div>
                                    <div class="row song">
                                      <div class="col-xs-2">
                                        <!--Player-->
                                        <div id="jquery_jplayer_'.$x.'" class="cp-jplayer"></div>
                                        <div id="cp_container_'.$x.'" class="cp-container">
                                          <div class="cp-buffer-holder">
                                            <div class="cp-buffer-1"></div>
                                            <div class="cp-buffer-2"></div>
                                          </div>
                                          <div class="cp-progress-holder">
                                            <div class="cp-progress-1"></div>
                                            <div class="cp-progress-2"></div>
                                          </div>
                                          <div class="cp-circle-control"></div>
                                          <ul class="cp-controls">
                                            <div class="cp-controlsli"><a class="cp-play" tabindex="1">play</a></div>
                                            <div class="cp-controlsli"><a class="cp-pause" style="display:none;" tabindex="1">pause</a></div>
                                          </ul>
                                        </div>
                                        <!--/Player-->
                                      </div>
                                      <div class="col-xs-8">
                                        <div class="title-song">'.$value->cancion.'</div>
                                        <div class="art-song">'.$value->artista.'</div>
                                      </div>
                                      <div class="col-xs-2">
                                         <div class="counter hidden-xs">
                                           <span>'.$value->cantidad.'</span>
                                         </div>
                                      </div>
                                    </div>
                                </figcaption>
                              </figure>
                              <!--/Item Large-->
                            </li>';

                    if(!empty($value->audio))
                    {
                        $lista['js'] .= 'var myCirclePlayer = new CirclePlayer("#jquery_jplayer_'.$x.'",
                                        {
                                          mp3: "'.$value->audio.'"
                                        }, {
                                          supplied: "mp3",
                                          cssSelectorAncestor: "#cp_container_'.$x.'",
                                          swfPath: "lib/jplayer",
                                          wmode: "window"
                                        });';
                    }              
                    
                   $x++;
                }
                   
            }

        }
       
        return $lista;
    }

    function getCancionesVideo($id_categoria)
    {
        $file = json_decode(file_get_contents('json/canciones.json'));
        $lista = array();                
        $button = '';
       
        
        if(is_array($file))
        {
            shuffle($file);             
            $x = 1;

            foreach ($file as $key => $value)
            {      
              
                
                if($value->id_categoria == $id_categoria)
                {
                                      
                    if(!empty($_SESSION['idins_premios']))
                    {
                       
                        $button = '<form action="action.php" method="post" role="form" id="form">
                        <input id="action" name="action" type="hidden" value="votar"/>
                        <input id="id_nominado" name="id_nominado" type="hidden" value="'.$value->id.'"/>
                        <input id="id_categoria" name="id_categoria" type="hidden" value="'.$id_categoria.'"/>
                        <input id="url" name="url" type="hidden" value="interna"/>

                        <button class="btn btn-vote navbar-btn button button--nina button--round-l button--text-thick button--inverted" data-text="VOTAR AHORA" onclick="javascript: ga(\'send\', \'event\', \'Premios Moda 2017\', \'Votar\', \''.$id_categoria.'\', \''.$value->id.'\');"><span>V</span><span>O</span><span>T</span><span>A</span><span>R</span><span>&nbsp;</span><span>A</span><span>H</span><span>O</span><span>R</span><span>A</span></button>

                        </form>';
                    }
                    else
                    {                      

                        $button = '<button class="btn btn-vote navbar-btn button button--nina button--round-l button--text-thick button--inverted" data-text="VOTAR AHORA" data-toggle="modal" data-target="#login" onclick="javascript: ga(\'send\', \'event\', \'Premios Moda 2017\', \'Votar\', \''.$id_categoria.'\', \''.$value->id.'\');"><span>V</span><span>O</span><span>T</span><span>A</span><span>R</span><span>&nbsp;</span><span>A</span><span>H</span><span>O</span><span>R</span><span>A</span></button>';
                    }

                    
                    $lista['html'] .= '<div class="col-md-4 col-sm-6 col-xs-12 item">
                                          <div class="text-center box-songs">
                                            <div class="embed-responsive embed-responsive-16by9">
                                              <iframe id="player2" class="embed-responsive-item" src="https://www.youtube.com/embed/'.$value->video.'?rel=0&wmode=Opaque&enablejsapi=1;showinfo=0;controls=0" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div class="line"></div>
                                            <div class="song">
                                              <h3 class="title-song">'.$value->cancion.'</h3>
                                              <div class="artist-song">'.$value->artista.'</div>
                                            </div>
                                            <div class="counter">
                                              <span>'.$value->cantidad.'</span>
                                            </div>
                                            <div class="btn-vota text-center">
                                            '.$button.'
                                            </div>
                                            <div class="clearfix"></div>
                                          </div>
                                        </div>';
                    
                    
                }          
                
                   
            }

        }
        return $lista;
    }


    function registrarVoto($id_inscrito, $campania, $id_nominado, $id_categoria, $fecreg, $horreg)
    {
        $votacion = array();
        $votacion['inscrito_id'] = $id_inscrito;
        $votacion['campania_id'] = mysql_real_escape_string($campania);
        $votacion['multimedia_id'] = $id_nominado;
        $votacion['categoria_id'] = $id_categoria;
        $votacion['fecha'] = $fecreg;
        $votacion['hora'] = $horreg;
        $id_votacion = $this->insert("votaciones", $votacion);

        return $id_votacion;

    }

}

?>