<!-- Términos y condiciones MODAL-->
    <div class="modal fade" id="TermsCond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
            <h4 class="modal-title" id="myModalLabel">Términos y Condiciones</h4>
          </div>





          <div class="modal-body">
            <h5 style="font-weight: bold; text-align: center;">Mi personaje favorito de Qumbias y Risas de Edwin Sierra</h5>
            <p align="center"><!-- <strong>Justin</strong> -->
     
      
              <span>Nueva Q, QQQumbia, presenta “Mi personaje favorito de Qumbias y Risas de Edwin Sierra”, la premiación más divertida del año. Podrás elegir a la mejor imitación de Edwin Sierra en su programa Qumbias y Risas de Radio Nueva Q. El 30 de noviembre anunciaremos en vivo al personaje ganador. Además, al votar estarás participando del sorteo de packs de electrodomésticos y canastas Q.</span>
              <br>
              <br>
            </p>


            <strong><u>Puedes participar:</u></strong>
            
            <ol style="margin:4px 0 20px 13px; list-style: disc;">

              <li>Ingresa a <a href="https://radionuevaq.pe/" target="_blank">radionuevaq.pe</a>, busca la imagen del concurso, ingresar al minisitio “Mi personaje favorito de Qumbias y Risas de Edwin Sierra”, regístrate y vota por tu preferido. </li>
              <br>
             

            </ol>

            <span>Consideraciones:</span>

            <ul style="margin:4px 0 20px 17px;  list-style: decimal;">
              <li>
                Sólo podrás registrarte una vez y votar sólo en ese momento hasta por 5 personajes. 
              </li>

              <li>
                Todas las personas que se hayan registrado, ingresarán a la base de datos para el sorteo.
              </li>

              <li>
                Podrás registrarte hasta las 12:00 pm del 29 de noviembre del 2017. 
              </li>

              <li>
                Si no completas todos los datos solicitados en el formulario y/o estos datos no son correctos o verdaderos podrás ser descalificado en cualquier momento y no podrás ser acreedor de ningún premio.
              </li>

              <li>En caso de detectarse actividad anormal relacionada con el uso del formulario de participación, o en caso de detectarse el uso de técnicas fraudulentas para la generación automática de votos se penalizará a dicho participante cancelando su registro, eliminándolo del concurso sin previo aviso, y bloqueando su participación en el resto de la campaña, no pudiendo ser acreedor de ningún premio.</li>

              <li>Los nombres de los ganadores del sorteo serán anunciados vía la señal de Radio Nueva Q el mismo día viernes 30 de noviembre a partir de las 7 a.m., durante el programa de Qumbias y Risas. Además, la lista de ganadores será publicada el 30 de noviembre a partir de las 1 p.m. en la sección de concursos de radionuevaq.pe y en el fanpage de Nueva Q. No se llamará telefónicamente a ningún ganador.</li>

            </ul>


            <p><strong><u>Ámbito de la promoción</u>: </strong>Lima Metropolitana y Callao</p>
            <br>

            <p><strong><u>Vigencia</u>: </strong>Del 15 hasta el 29 de noviembre de 2017.</p>
            <br>

            <p><strong><u>Fecha del sorteo</u>: </strong>29 de noviembre del 2017 a las 3:00 pm</p>
            <br>
            
            <p><strong><u>Premios (Stock)</u>: </strong></p>
            <ul style="margin:4px 0 20px 17px;  list-style: disc;">
              <li>Ganador 1: Pack de 1 Sanguchera + Hervidor</li>
              <li>Ganador 2: Pack de 1 Sanguchera + Canasta Q</li>
              <li>Ganador 3: Pack de 1 Plancha + Canasta Q</li>
              <li>Ganador 4: Pack de 1 Sanguchera + Hervidor</li>
            </ul>
            

             <p><strong><u>Entrega de premios</u>: </strong></p>
             <div style="margin:4px 0 20px 0px;">
               <p>El viernes 01 de diciembre del 2017 de 10:00 am a 03:30 pm en Calle Juan Nepomuceno 147, Chorrillos, Lima. El ganador deberá acercarse con su DNI original + 2 copias al momento del recojo y/o entrega de su premio. Único día y horario de entrega.</p>
               <br>
               <p>Si el ganador no puede venir a recoger su premio, puede enviar a un representante con una carta poder simple que tenga la firma y huella digital del ganador así como con su DNI original + 2 copias y con copia del DNI del ganador. </p>
               <br>
               <p>La radio no asumirá ningún gasto (sea por transporte, alojamiento, etc.) para el recojo del premio.</p>

               <p>En caso que a criterio de nuestra Empresa se detecte algún caso de intento de suplantación de identidad (o que una persona intente hacerse pasar por algún ganador) y/o que se han brindado datos falsos ya sea al momento de participar o al momento de reclamar su premio, nuestra Empresa iniciará las acciones legales que correspondan (incluso penales) en contra de dichas personas.</p>
             </div>




          </div>
          <div class="modal-footer"></div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->