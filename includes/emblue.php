<?php
class Emblue{
	
	private $token = null;

	function __construct()
	{
		$this->token = $this->connection();
	}


	function connection(){

		$curl = curl_init('http://api.embluemail.com/Services/EmBlue3Service.svc/Json/Authenticate');

		$curl_post_data = array(
		'Token' => "yAO7qBbI-v8owx-1wds5-lzCVbZ6Jmk",
		'User' => "nuevosmedios@crp.pe",
		'Pass' => "P@8!]7qCG!I0" );

		$jsonArray=json_encode($curl_post_data);


		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_POST, true); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonArray); 
		$curl_response = curl_exec($curl);


		$jason= json_decode($curl_response);

		$token = $jason->Token;
		return $token;

	}

	function addContact($id_campania, $nombre, $email, $apellidos, $telefono, $dni, $sexo, $cumpleanios)
	{
		$curl = curl_init ('http://api.embluemail.com/Services/EmBlue3Service.svc/Json/NewContact');
		$token = $this->token;

		$curl_post_data = array( 
			'Token' => $token,
			'Email' => $email,
			'EditCustomFields' => "nombre:|:".$nombre.":|:1|||apellido:|:".$apellidos.":|:1|||telefono_1:|:".$telefono.":|:1|||dni:|:".$dni.":|:1|||sexo:|:".$sexo.":|:1|||cumpleanios:|:".$cumpleanios.":|:1",
			'SelectGroups' => "307804;170249"  );

		$jsonArray=json_encode($curl_post_data);

		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonArray);
		$curl_response = curl_exec($curl);

		$jason= json_decode($curl_response); 
		return $jason;
	}

	function sendMail($id_inscrito, $nombre)
	{
		$curl = curl_init ('http://api.embluemail.com/Services/EmBlue3Service.svc/Json/SendMail');
		$token = $this->token;

		$curl_post_data = array( 
			'Token' => $token,
			'Emails' => array ($id_inscrito),
			'ActionId' => "444161",
			'Message' => $nombre." Te registraste exitosamente. Gracias por participar",
			'Subject' => "¡Gracias por participar!"  );

		$jsonArray=json_encode($curl_post_data);

		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonArray);
		$curl_response = curl_exec($curl);

		$jason= json_decode($curl_response); 
		return $jason;
	}

	
}