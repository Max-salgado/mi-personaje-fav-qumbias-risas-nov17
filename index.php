<?php 
require_once("includes/conn.php");
require_once("includes/tools.php");

$tools = new Tools();
?> 

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mi personaje favorito de ‘Qumbias y Risas’ de Edwin Sierra</title>

	<meta name="description" content="Vota por tu personaje favorito de Edwin Sierra en ‘Qumbias y Risas’ y podrás participar por muchos premios. Sorteo: 29 de noviembre." />
    <meta name="keywords" content="Nueva Q, cumbia, concursos, Edwin Sierra, concursos Perú, radio peruana, cumbia peruana, artistas de la cumbia, radio, radio cumbia, Nueva Q FM, QQQumbia, cómico peruano, cómico, Cumbias y Risas, Qumbias y Risas." />   

    <meta property="og:title" content="Mi personaje favorito de ‘Qumbias y Risas’ de Edwin Sierra" />
    <meta property="og:description" content="Vota por tu personaje favorito de Edwin Sierra en ‘Qumbias y Risas’ y podrás participar por muchos premios. Sorteo: 29 de noviembre."/>
    <meta property="og:url" content="http://concursos.crp.pe/nuevaq/campanias/minisites/mi-personaje-fav-qumbias-risas-nov17/" />
    <meta property="og:image" content="http://concursos.crp.pe/nuevaq/campanias/minisites/mi-personaje-fav-qumbias-risas-nov17/assets/img/facebook560.jpg" />


	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/main.js?v9"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/estilos-min.css?v4">

	<link href="assets/css/validation.css" rel="stylesheet">
    <script type="text/javascript" src="assets/js/validation.js?v=38"></script>

    <link rel="icon" type="image/png" href="https://radionuevaq.pe/assets/favicons/favicon-16x16.png" sizes="16x16">


</head>
<body>
	<!-- <div class="parche"></div> -->
	<?php include ('includes/superior.php'); ?>
	

	<form action="action.php" method="post" onSubmit="return valRegistro();">
    <input id="action" name="action" type="hidden" value="registro"/> 

	<section id="contenedor-principal">

		<article class="contenedor-interno container-fluid">
			

			<header class="cp-superior row"> 

				<div class="logo-q col-md-1 col-sm-1">
					<a href="https://radionuevaq.pe/" target="_blank"><img src="assets/img/logo-nuevaq.png" alt="Logo Nueva Q" class="img-responsive center-block"></a>
				</div>

				<div class="logo-campania col-md-5 col-sm-11">
					<a href="http://concursos.crp.pe/nuevaq/campanias/minisites/mi-personaje-fav-qumbias-risas-nov17/"><img src="assets/img/logo-campania.png" alt="Logo de la campaña" class="img-responsive center-block in-home"></a>
				</div>




				<div class="enunciado-principal col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-1">
					<h1 class="titulo">
						<img src="assets/img/titulo-campania.png" alt="Título campaña" class="img-responsive center-block">
					</h1>
				
					<p class="parrafo parrafo1"><span>Vota</span> por tu <span> personaje favorito</span></p>
					
					<p class="parrafo parrafo3">*Puedes elegir hasta 5 personajes</p>
				
				</div>



				<div class="premios-principal col-md-2 col-sm-2">
					<img src="assets/img/premios.png" alt="Premios" class="img-responsive center-block">					
				</div>


			</header> <!-- fin cp-superior -->



			<div class="cp-inferior col-md-10 col-md-offset-1">	
					<div class="margen-inferior">
						<div class="contenedor-personaje">
							
							<div class="imagen-personaje" data-target="#mostrar-video1" data-toggle="modal">
								<img src="assets/img/personajes/1.png" alt="Cantinflas" class="img-responsive center-block">
							</div>

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'Cantinflas');" value="1048">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>

						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video2" data-toggle="modal">
								<img src="assets/img/personajes/2.png" alt="Choledo" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'Choledo');" value="1049">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>


						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video3" data-toggle="modal">
								<img src="assets/img/personajes/3.png" alt="El Chino" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'El Chino');" value="1050">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>


						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video4" data-toggle="modal">
								<img src="assets/img/personajes/4.png" alt="El Patrón" class="img-responsive center-block">
								
								

							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'El Patrón');" value="1051">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>


						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video5" data-toggle="modal">
								<img src="assets/img/personajes/5.png" alt="La Fuana" class="img-responsive center-block">
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'La Fuana');" value="1052">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>
					</div>

					<!-- /**/ -->
					
					<div>
						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video6" data-toggle="modal">
								<img src="assets/img/personajes/6.png" alt="Lalito" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'Lalito');" value="1053">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>




						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video7" data-toggle="modal">
								<img src="assets/img/personajes/7.png" alt="La tía Peta" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'La tía Peta');" value="1054">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>




						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video8" data-toggle="modal">
								<img src="assets/img/personajes/8.png" alt="La Fuana" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'La Fuana');" value="1055">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>



						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video9" data-toggle="modal">
								<img src="assets/img/personajes/9.png" alt="La Fuana" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'Orificio');" value="1056">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>





						<div class="contenedor-personaje">

							<div class="imagen-personaje" data-target="#mostrar-video10" data-toggle="modal">
								<img src="assets/img/personajes/10.png" alt="La Fuana" class="img-responsive center-block">

								
							</div>

							

							<div class="op-gen">
								<input type="checkbox" name="respuesta[]" class="seleccionados" onclick="valPersonaje(); ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Ver video', 'Ppkuy');" value="1057">
								<button class="boton-personaje boton-votar">
									<p>Votar</p>
								</button>
							</div>

						</div>

						
					</div>

					<span id="error_personajes"></span>

					<div class="ir-formulario"><a href="#registro">Registra tu voto</a></div>

			</div> <!-- fin cp-inferior -->
			
			<div class="contenedor-legal">
				<a href="#"  data-target="#TermsCond" data-toggle="modal">Ver términos y condiciones</a><p> | SORTEO 29 DE NOVIEMBRE</p>
			</div>
			
		</article>
	</section> <!-- fin contenedor-principal -->




	<div id="formulario">
		<div class="contenedor-interno">
			<div class="contenedor-formulario row" id="registro">

				

					<div class="caja-form col-md-4 col-sm-6">
						<label>Nombres y apellidos:</label>
						<input class="data-usuario" placeholder="Coloca tus nombres y apellidos" type="text"  id="nombres" name="nombres" required>
						<span id="error_nombres"></span>
					</div>

					<!-- <div class="caja-form col-md-4 col-sm-6">
						<label>Apellidos:</label>
						<input class="data-usuario" placeholder="Coloca tus apelldios" type="text" id="apellidos" name="apellidos" required>
						<span id="error_apellidos"></span>
					</div> -->

					<div class="caja-form col-md-4 col-sm-6">
						<label>Dni:</label>
						<input class="data-usuario" placeholder="Coloca tu Dni" type="respuesta" id="dni" name="dni" required maxlength="8">
						<span id="error_dni"></span>
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<label>E-mail:</label>
						<input class="data-usuario" placeholder="Coloca tu E-mail" type="email" id="email" name="email" required>
						<span id="error_email"></span>
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<label>Teléfono:</label>
						<input class="data-usuario" placeholder="Coloca tu teléfono" type="tel" id="telefono" name="telefono" required maxlength="9">
						<span id="error_telefono"></span>
					</div>

					<div class="caja-form col-md-4 col-sm-6">
						<button class="btn-data-usuario" onclick="ga('send', 'event', 'Personaje favorito Qumbias Risas 2017', 'Registrar');">
							<p>
								Enviar
							</p>
						</button>
					</div>


					<div class="col-sm-12 aceptar-campos">
			        	<div class="checkbox">
			                  <input type="checkbox" value="1" name="informacion" id="informacion">
			                  <label for="informacion">Deseo recibir información de Radio  Nueva Q y sus anunciantes.</label>

			              </div>
			              <div class="checkbox">
			                  <input type="checkbox" value="1" name="titular" id="titular">
			                  <label for="titular">Acepto los <a href="#" id="termiCond" data-toggle="modal" data-target="#TermsCond">Términos y Condiciones</a><span id="error_tyc"></span></label>
			              </div>
			        </div>

				

			</div> <!-- fin contenedor-formulario -->
		</div>

	</div> <!-- fin formulario -->

	</form>

	<?php include ('includes/pie.php'); ?>
	<?php include ('includes/contenedor-video.php'); ?>
	<?php include ('includes/tyc.php'); ?>

	<?php include ('includes/limite.php'); ?>

	<footer id="pie">
		<div class="contenedor-interno"></div>
	</footer> <!-- pie -->
	
	 <?php
        $msg = NULL;
        if(!empty($_GET['msg']))
        {
          $msg = $_GET['msg'];
        }

        if(!empty($msg)) :
        switch ($msg) {

            default:
              $modal = '#'.$msg;
            break;
            
        }
        ?>
        <script type="text/javascript">
          $('<?php echo $modal; ?>').modal('show');
        </script>

    <?php endif; ?>

	
<!-- Begin comScore DAx STANDARD-->
<script type="text/javascript">
  // <![CDATA[
  function udm_(a){var b="comScore=",c=document,d=c.cookie,e="",f="indexOf",g="substring",h="length",i=2048,j,k="&ns_",l="&",m,n,o,p,q=window,r=q.encodeURIComponent||escape;if(d[f](b)+1)for(o=0,n=d.split(";"),p=n[h];o<p;o++)m=n[o][f](b),m+1&&(e=l+unescape(n[o][g](m+b[h])));a+=k+"_t="+ +(new Date)+k+"c="+(c.characterSet||c.defaultCharset||"")+"&c8="+r(c.title)+e+"&c7="+r(c.URL)+"&c9="+r(c.referrer),a[h]>i&&a[f](l)>0&&(j=a[g](0,i-8).lastIndexOf(l),a=(a[g](0,j)+k+"cut="+r(a[g](j+1)))[g](0,i)),c.images?(m=new Image,q.ns_p||(ns_p=m),m.src=a):c.write("<","p","><",'img src="',a,'" height="1" width="1" alt="*"',"><","/p",">")}
  udm_('http'+(document.location.href.charAt(4)=='s'?'s://sb':'://b')+'.scorecardresearch.com/p?c1=2&c2=6906600&ns_site=nuevaq-radio&name=concursos.minisites.personaje-favorito-qumbias-risas-nov17.home');
  // ]]>
</script>
<noscript>
  <p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6906600&amp;ns_site=nuevaq-radio&amp;name=concursos.minisites.personaje-favorito-qumbias-risas-nov17.home" height="1" width="1" alt="*"></p>
</noscript>
<!-- End comScore DAx STANDARD -->
<script language="JavaScript1.3" src="http://b.scorecardresearch.com/c2/6906600/ct.js"></script>
<script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39226895-1', 'auto');
  ga('send', 'pageview');
</script> 


</body>
</html>



